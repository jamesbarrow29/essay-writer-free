**Healthcare Budgeting**

Advantages in Healthcare Budgeting

Speaking about the Healthcare Budgeting and analyzing it in different countries it is rather easy to find a lot of defects, mistakes, specific features and crying injustice. But in general any government forms the budgeting of its country according to its own principles and needs of higher or vice versa lower financing of this or that program or fund. Depicting the condition of the USA budget distribution (assessment) it is interesting to observe the state of affairs in Healthcare Budget 2006.

![](https://i.pinimg.com/564x/3f/d1/9a/3fd19a7596534f2b85f7c03d188618ba.jpg)

The Office of Management and Budget inform with some kind of pride that promoting the valuable accessible healthcare [medical assignment help](https://essaywriterfree.net/medical-assignment-help/) is of vital importance nowadays. No doubt they admit:

America’s health system provides high-quality, leading-

edge care to those who need it. But rising costs can put

health care coverage out of reach for many Americans,

imposing a burden on families and businesses. That is why

the President’s health care policies focus on making

health care more affordable and accessible, especially for

low-income Americans.

Moreover they underline that that the Healthcare Budget for the year 2006 has a lot of advantages. They speak about the improvement of the situation, noting different funds for people with various disabilities, 26 million of dollars for the funds of high poverty countries; they also note about 1.200 new centers that are going to be created. But why don not they write about the pruning of Healthcare Budget, about the lessening of some other programs’ financing.

Pruning of Healthcare Budget

But what are the main ideas of this budget? Kaisernetwork.org shed light on it:

The $2.7 trillion budget proposal includes $36 billion in spending reductions for Medicare over five years. The Medicare spending reductions would total $105 billion over 10 years. Bush’s Medicare proposal would reduce spending on the program - which the Congressional Budget Office estimates at $2.56 trillion over the next five years – by about 1.4%. Many of Bush’s proposals follow recommendations from the Medicare Payment Advisory Commission, an independent federal panel. Bush’s proposal aims to eliminate or reduce spending in 141 programs for savings of $14.4 billion in 2007.

The USA Congress approved the budgetary plan for the financial year 2006 which was offered by the president George Bush at the rate of 2,6 trillion of dollars. Kaisernetwork.org cites that his main goal is: “to focus on national priorities and tighten our belt Elsewhere”. That is why the key positions of this document deal with cutting down of expenses. So, it was the first time since the year 1997 when it was assumed to cut down budgeting of the program on the health protection Medicaid (providing medical service for poor citizens) on 10 milliards of dollars.

The American Senate delivered an unexpected blow to the plans of Bush administration about shortening of budget deficit. The Associated Press reported that the upper chamber of Congress of the USA accepted a budget at the rate of a 2,6 trillion of dollars, refusing to decrease expenses for the social programs, including medical service, municipal needs, and also primary and middle education.

Firstly the House of Representatives approved the serious cutback of expenses for medical help and other projects, related to public welfare [biology homework help](https://essaywriterfree.net/biology-homework-help/), which were offered by Bush. But unexpectedly for everybody the Senate refused to reduce financing the majority of these programs. At the same time, they voted for the considerable decline of the tax loading and as a result state budget would receive less than 135 milliards of dollars during the next five years - it practically twice as much the republicans from the lower chamber of Congress demanded.

![](https://i.pinimg.com/564x/2e/ec/7b/2eec7b4244b296c9e640ca8131813fe6.jpg)

As a result the Senate voted for the increase of charges as opposed to what the White House insisted on. In particular, all present programs beginning from the fight against AIDS and up to the projects of water-supply improvement got some additional funds.

But coming back to the question of Medicare program financing, which was depicted in George Bush’s 2006 Budget, there are a lot of misunderstandings and disagreements between the President’s view and the opinions of different observers. Kaisernetwork.org gives a lot of comments developing this question. For example Barbara Kennelly, the president of the National Committee to Preserve Social Security and Medicare, indicated that: “…the president’s budget proposal is “an effort to the fundamentally change the way we look at Medicare”.

Dick Davidson, the president of the American Hospital Association, replied that “…Bush’s budget is “a step backwards in protecting access to care for all Americans”.

Bill Novelli, the president of AARP, presented his own view: “Arbitrary caps on Medicare will mean that providers or beneficiaries will have to make up the difference through lower payment rates or higher cost sharing”.

Making a conclusion about this topic it should be admitted that pruning of Healthcare Budget is a rather hard and delicate question, that needs a lot of financial and economic knowledge and experience. Everything (all the advantages and disadvantages) must thought-out twice and beforehand. Moreover to my mind it is not rational to decrease financing of such important sphere as Healthcare, as the health situation of the population is one of the main indexes of country’s today’s and future prosperity and success.

**Referencing posts:**

[https://www.vingle.net/posts/4059785](https://www.vingle.net/posts/4059785)

[https://telegra.ph/Women-and-Literature-10-14](https://telegra.ph/Women-and-Literature-10-14)

[https://lookingforclan.com/clans/spiritual-values-modern-education](https://lookingforclan.com/clans/spiritual-values-modern-education)

[https://localendar.com/event?DAM=PublishedEvent&m=610398&event_id=2&calendar_id=610398](https://localendar.com/event?DAM=PublishedEvent&m=610398&event_id=2&calendar_id=610398)